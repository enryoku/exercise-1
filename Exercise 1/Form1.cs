﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            MessageBox.Show("You checked a box!", "Checkbox");
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            string radioButton = "";
            if(optionOne.Checked)
            {
                radioButton = optionOne.Text;
            }
            else if(optionTwo.Checked)
            {
                radioButton = optionTwo.Text;
            }
            else if(optionThree.Checked)
            {
                radioButton = optionThree.Text;
            }
            MessageBox.Show(radioButton + " was selected!", myDateTimePicker.Value.ToString());
        }

        private void DateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void OptionOne_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void OptionTwo_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void OptionThree_CheckedChanged(object sender, EventArgs e)
        {
            
        }
    }
}
