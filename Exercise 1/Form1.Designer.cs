﻿namespace Exercise_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.myButton = new System.Windows.Forms.Button();
            this.myCheckBox = new System.Windows.Forms.CheckBox();
            this.myDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.optionOne = new System.Windows.Forms.RadioButton();
            this.optionTwo = new System.Windows.Forms.RadioButton();
            this.optionThree = new System.Windows.Forms.RadioButton();
            this.myInstructions = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // myButton
            // 
            this.myButton.Location = new System.Drawing.Point(241, 9);
            this.myButton.Name = "myButton";
            this.myButton.Size = new System.Drawing.Size(75, 23);
            this.myButton.TabIndex = 0;
            this.myButton.Text = "Click ME!";
            this.myButton.UseVisualStyleBackColor = true;
            this.myButton.Click += new System.EventHandler(this.Button1_Click);
            // 
            // myCheckBox
            // 
            this.myCheckBox.AutoSize = true;
            this.myCheckBox.Location = new System.Drawing.Point(333, 15);
            this.myCheckBox.Name = "myCheckBox";
            this.myCheckBox.Size = new System.Drawing.Size(65, 17);
            this.myCheckBox.TabIndex = 1;
            this.myCheckBox.Text = "CHECK!";
            this.myCheckBox.UseVisualStyleBackColor = true;
            this.myCheckBox.CheckedChanged += new System.EventHandler(this.CheckBox1_CheckedChanged);
            // 
            // myDateTimePicker
            // 
            this.myDateTimePicker.Location = new System.Drawing.Point(12, 12);
            this.myDateTimePicker.Name = "myDateTimePicker";
            this.myDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.myDateTimePicker.TabIndex = 2;
            this.myDateTimePicker.ValueChanged += new System.EventHandler(this.DateTimePicker1_ValueChanged);
            // 
            // optionOne
            // 
            this.optionOne.AutoSize = true;
            this.optionOne.Checked = true;
            this.optionOne.Location = new System.Drawing.Point(12, 50);
            this.optionOne.Name = "optionOne";
            this.optionOne.Size = new System.Drawing.Size(45, 17);
            this.optionOne.TabIndex = 3;
            this.optionOne.TabStop = true;
            this.optionOne.Text = "One";
            this.optionOne.UseVisualStyleBackColor = true;
            this.optionOne.CheckedChanged += new System.EventHandler(this.OptionOne_CheckedChanged);
            // 
            // optionTwo
            // 
            this.optionTwo.AutoSize = true;
            this.optionTwo.Location = new System.Drawing.Point(12, 73);
            this.optionTwo.Name = "optionTwo";
            this.optionTwo.Size = new System.Drawing.Size(46, 17);
            this.optionTwo.TabIndex = 4;
            this.optionTwo.Text = "Two";
            this.optionTwo.UseVisualStyleBackColor = true;
            this.optionTwo.CheckedChanged += new System.EventHandler(this.OptionTwo_CheckedChanged);
            // 
            // optionThree
            // 
            this.optionThree.AutoSize = true;
            this.optionThree.Location = new System.Drawing.Point(12, 96);
            this.optionThree.Name = "optionThree";
            this.optionThree.Size = new System.Drawing.Size(53, 17);
            this.optionThree.TabIndex = 5;
            this.optionThree.Text = "Three";
            this.optionThree.UseVisualStyleBackColor = true;
            this.optionThree.CheckedChanged += new System.EventHandler(this.OptionThree_CheckedChanged);
            // 
            // myInstructions
            // 
            this.myInstructions.Location = new System.Drawing.Point(96, 47);
            this.myInstructions.Multiline = true;
            this.myInstructions.Name = "myInstructions";
            this.myInstructions.Size = new System.Drawing.Size(435, 50);
            this.myInstructions.TabIndex = 6;
            this.myInstructions.Text = resources.GetString("myInstructions.Text");
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 188);
            this.Controls.Add(this.myInstructions);
            this.Controls.Add(this.optionThree);
            this.Controls.Add(this.optionTwo);
            this.Controls.Add(this.optionOne);
            this.Controls.Add(this.myDateTimePicker);
            this.Controls.Add(this.myCheckBox);
            this.Controls.Add(this.myButton);
            this.Name = "Form1";
            this.Text = "Exercise 1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button myButton;
        private System.Windows.Forms.CheckBox myCheckBox;
        private System.Windows.Forms.DateTimePicker myDateTimePicker;
        private System.Windows.Forms.RadioButton optionOne;
        private System.Windows.Forms.RadioButton optionTwo;
        private System.Windows.Forms.RadioButton optionThree;
        private System.Windows.Forms.TextBox myInstructions;
    }
}

